#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

alias exa='exa -al --color=always --group-directories-first'
alias ls='ls --color=auto'
PS1='[\u@\h \W]\$ '
neofetch
